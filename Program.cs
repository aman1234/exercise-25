﻿using System;

namespace exercise_25
{
    class Person
    {
     string Name;
     int Age;

     public Person(string _name, int _age )
     {
        Name = _name; 
        Age = _age;
     }

       public void SayHello()
        {
            Console.WriteLine($"Hello World!");
            Console.WriteLine($"My name is {Name} and i am {Age}!");
        }
    }
    public class Program
    {

        public static void Main(string[] args)
        {
            var aman = new Person("aman",20);
   
            aman.SayHello();
        }
    }
}
